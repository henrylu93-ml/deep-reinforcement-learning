import algorithms.ddpg as ddpg
import algorithms.randomAgent as randomAgent
import gym

# Set the hyperparameters to use here
hyper = ddpg.hyper  # Get the default hyperparameters first
hyper['gamma'] = 0.99  # discount factor
hyper['lrActor'] = 0.00001  # learning rate of actor model
hyper['lrCritic'] = 0.0001  # learning rate of critic model
hyper['lrDecay'] = 1  # decay applied to both learning rates every episode
hyper['trActor'] = 0.005  # rate at which target actor tracks learned model
hyper['trCritic'] = 0.005  # rate at which target critic tracks learned model
hyper['noiseSigma'] = 0.2  # Standard deviation of noise term
hyper['noiseDecay'] = 1  # decay applied to noise every episode
hyper['batchSize'] = 64  # Size of minibatch to feed to network
hyper['updateEvery'] = 64  # Update network every x steps
hyper['nHidden'] = 256  # number of hidden layer nodes
hyper['maxSteps'] = 3000  # Max number of steps per episode
hyper['trainAfter'] = 10000  # begin updates after x steps
hyper['replayBufferSize'] = 100000  # Max size of replay buffer
hyper['trainEpisodes'] = 1000  # Number of episodes to train for
hyper['testEpisodes'] = 10  # Number of episodes to test for

# Create gym environment
env = gym.make('LunarLanderContinuous-v2')

# Create RL agent using algorithm of choice
agent = ddpg.DDPG(env, render=False, hyper=hyper)
# agent = randomAgent.RandomAgent(env, render=False, hyper=hyper)

# Set hyperparameters if desired
# Then reset models, evaluate, and plot
agent.hyper['batchSize'] = 16  # Size of minibatch to feed to network
agent.resetModels()
agent.evaluate()
agent.saveRewards('LunarLanderContinuous.txt')
agent.plotTestRewards("batchSize=16")

agent.hyper['batchSize'] = 64  # Size of minibatch to feed to network
agent.resetModels()
agent.evaluate()
agent.saveRewards('LunarLanderContinuous.txt')
agent.plotTestRewards("batchSize=64")

agent.hyper['batchSize'] = 256  # Size of minibatch to feed to network
agent.resetModels()
agent.evaluate()
agent.saveRewards('LunarLanderContinuous.txt')
agent.plotTestRewards("batchSize=256")

# At the end of the script, display the plot
agent.showPlot()
