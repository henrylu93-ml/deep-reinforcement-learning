Description
===
This project contains deep reinforcement learning algorithms to be trained and tested on OpenAI Gym environments.

Contents
===
- *algorithms/* - contains the implemented RL algorithms
- the base folder contains scripts for running the algorithms in specific environments

Installation
===
This project requires the following libraries and frameworks:
- [OpenAI Gym](https://github.com/openai/gym)
- [PyTorch](https://pytorch.org/get-started/locally/)
- matplotlib ```pip install --user matplotlib```
- numpy ```pip install --user numpy```

For Box2D environments (e.g., LunarLander):
- Box2D ```pip install --user Box2D```

For MuCoCo-based environments (e.g., HalfCheetah, Fetch)
- [MuCoCo physics engine (license required)](http://www.mujoco.org/)
- [mujoco_py](https://github.com/openai/mujoco-py) (for windows, follow these [build instructions](https://github.com/aaronsnoswell/mujoco-py/blob/7d2fa8f2e3d031a3e25803fc8a9e908732a22a58/README.windows-conda.md))

Follow the links and commands provided above to install the corresponding libraries in your environment.