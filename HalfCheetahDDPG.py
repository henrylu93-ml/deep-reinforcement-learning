import algorithms.ddpg as ddpg
import gym

hyper = ddpg.hyper
hyper['gamma'] = 0.95  # discount factor
hyper['lrActor'] = 0.0005  # learning rate of actor model
hyper['lrCritic'] = 0.0005  # learning rate of critic model
hyper['lrDecay'] = 1  # decay applied to both learning rates every episode
hyper['trActor'] = 0.005  # rate at which target actor tracks learned model
hyper['trCritic'] = 0.005  # rate at which target critic tracks learned model
hyper['noiseSigma'] = 0.2  # Standard deviation of noise term
hyper['noiseDecay'] = 1  # decay applied to noise every episode
hyper['batchSize'] = 256  # Size of minibatch to feed to network
hyper['updateEvery'] = 64  # Update network every x steps
hyper['nHidden'] = 256  # number of hidden layer nodes
hyper['maxSteps'] = 1000  # Max number of steps per episode
hyper['trainAfter'] = 10000  # begin updates after x steps
hyper['replayBufferSize'] = 1000000  # Max size of replay buffer
hyper['trainEpisodes'] = 400  # Number of episodes to train for
hyper['testEpisodes'] = 10  # Number of episodes to test for

env = gym.make('HalfCheetah-v3')
agent = ddpg.DDPG(env, render=False, hyper=hyper)

agent.hyper['noiseSigma'] = 0.2  # Standard deviation of noise term
agent.resetModels()
agent.evaluate()
agent.saveRewards('HalfCheetahDDPG.txt')
agent.plotTestRewards("['noiseSigma'] = 0.2")

agent.showPlot()
