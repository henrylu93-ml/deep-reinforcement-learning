import torch
import numpy as np
import copy
from algorithms.base import Base
from collections import deque

# These are the default hyperparameters for this algorithm.
hyper = {}
hyper['gamma'] = 0.9  # discount factor
hyper['lrActor'] = 0.001  # learning rate of actor model
hyper['lrCritic'] = 0.001  # learning rate of critic model
hyper['lrDecay'] = 1  # decay applied to both learning rates every episode
hyper['trActor'] = 0.005  # rate at which target actor tracks learned model
hyper['trCritic'] = 0.005  # rate at which target critic tracks learned model
hyper['noiseSigma'] = 0.2  # Standard deviation of noise term
hyper['noiseDecay'] = 1  # decay applied to noise every episode
hyper['batchSize'] = 64  # Size of minibatch to feed to network
hyper['updateEvery'] = 50  # Update network every x steps
hyper['nHidden'] = 64  # number of hidden layer nodes
hyper['maxSteps'] = 1000  # Max number of steps per episode
hyper['trainAfter'] = 50000  # begin updates after x steps
hyper['replayBufferSize'] = 100000  # Max size of replay buffer
hyper['trainEpisodes'] = 1000  # Number of episodes to train for
hyper['testEpisodes'] = 10  # number of episodes to test for


class DDPG(Base):
    """
    This implements the Deep Deterministic Policy Gradient algorithm.
    """
    def __init__(self, env, render, hyper=hyper):
        super().__init__(env, render)
        self.hyper = hyper  # dict of hyperparameters

    def resetModels(self):
        """Create the actor and critic models, their target models, and optimizers."""
        self.testRewards = []  # Reset test rewards
        self.noiseSigma = self.hyper['noiseSigma']  # std dev for exploration noise
        self.episode = 0  # total episode count

        # Actor model has two hidden layers
        # Input is state, output is deterministic action
        self.actorModel = torch.nn.Sequential(
            torch.nn.Linear(self.dimState, self.hyper['nHidden']),
            torch.nn.ReLU(),
            torch.nn.Linear(self.hyper['nHidden'], self.hyper['nHidden']),
            torch.nn.ReLU(),
            # uncomment below to add another hidden layer
            # torch.nn.Linear(self.hyper['nHidden'], self.hyper['nHidden']),
            # torch.nn.ReLU(),
            torch.nn.Linear(self.hyper['nHidden'], self.dimAction),
            torch.nn.Tanh())
        self.actorModel.to(self.device)  # Set device (CPU or GPU)
        self.actorModel = self.actorModel.float()  # Set to use 32-bit float

        # Critic model has two hidden layers
        # Input is state-action pair, output is the Q-value of that pair.
        self.criticModel = torch.nn.Sequential(
            torch.nn.Linear(self.dimState + self.dimAction, self.hyper['nHidden']),
            torch.nn.ReLU(),
            torch.nn.Linear(self.hyper['nHidden'], self.hyper['nHidden']),
            torch.nn.ReLU(),
            # torch.nn.Linear(self.hyper['nHidden'], self.hyper['nHidden']),
            # torch.nn.ReLU(),
            torch.nn.Linear(self.hyper['nHidden'], 1),
        )
        self.criticModel.to(self.device)
        self.criticModel = self.criticModel.float()

        # Target models are initialized as copies of the learned models
        self.actorTarget = copy.deepcopy(self.actorModel)
        self.criticTarget = copy.deepcopy(self.criticModel)

        # Don't keep gradients for target models
        for params in self.actorTarget.parameters():
            params.requires_grad = False
        for params in self.criticTarget.parameters():
            params.requires_grad = False

        # initialize optimizers
        self.actorOpt = torch.optim.Adam(self.actorModel.parameters(),
                                         lr=self.hyper['lrActor'],
                                         eps=1e-04)
        self.criticOpt = torch.optim.Adam(self.criticModel.parameters(),
                                          lr=self.hyper['lrCritic'],
                                          eps=1e-04)

        # Replay buffer holds past transitions
        self.replayBuffer = deque(maxlen=self.hyper['replayBufferSize'])

    def storeReplay(self, state, action, reward, nextState, done):
        """
        Store a (state, action, reward, nextState, done) tuple in the replay buffer.
        """
        replay = (state, action, reward, nextState, done)
        self.replayBuffer.append(replay)

    def sampleReplay(self, batchSize):
        """
        Randomly sample batchSize replays from the replay buffers
        """
        randomIndices = np.random.randint(0, len(self.replayBuffer), size=batchSize)
        reState = torch.tensor([self.replayBuffer[i][0] for i in randomIndices],
                               device=self.device,
                               dtype=torch.float)
        reAction = torch.tensor([self.replayBuffer[i][1] for i in randomIndices],
                                device=self.device)
        reReward = torch.tensor([self.replayBuffer[i][2] for i in randomIndices],
                                device=self.device)
        reNextState = torch.tensor([self.replayBuffer[i][3] for i in randomIndices],
                                   device=self.device,
                                   dtype=torch.float)
        reDone = torch.tensor([self.replayBuffer[i][4] for i in randomIndices], device=self.device)
        return (reState, reAction, reReward, reNextState, reDone)

    def chooseAction(self, state, sigma=0.0):
        """
        Choose action based on policy.
        Action is a sample from a gaussian distribution where the policy is the mean.
        sigma (standard deviation) determines amount of exploration.
        """
        with torch.no_grad():  # sampling action should not affect gradient
            mu = self.actorModel(state)
            actionDistribution = torch.distributions.normal.Normal(mu, sigma)
        action = actionDistribution.sample()
        # Cutoff action at action space limits
        # This assumes all action dimensions have the same limits
        action = torch.clamp(input=action,
                             min=self.env.action_space.low[0],
                             max=self.env.action_space.high[0])
        return action

    def train(self, numEpisodes):
        """Train this algorithm on the environment"""

        for _ in range(numEpisodes):
            self.episode += 1

            state = torch.tensor(self.reset(), device=self.device).float()  # initial state
            done = False

            # Keep track of when updates should occur.
            updateCounter = self.hyper['updateEvery']
            numSteps = 0  # current step within episode
            totalRewards = 0  # total reward over a single episode

            while not done:
                numSteps += 1

                # Actions are random until replay buffer fills up sufficiently
                if len(self.replayBuffer) < self.hyper['trainAfter']:
                    action = torch.tensor(self.env.action_space.sample(), device=self.device)
                else:  # else sample action according to policy
                    action = self.chooseAction(state, sigma=self.noiseSigma)

                # Step in environment and store in replay buffer
                observation, reward, done, info = self.step(action.cpu().numpy())
                self.storeReplay(state.cpu().numpy(),
                                 action.cpu().numpy(), reward, observation,
                                 0 if numSteps == self.hyper['maxSteps'] else int(done))
                nextState = torch.tensor(observation, device=self.device).float()

                # Update models if replay buffer has filled up sufficiently
                if len(self.replayBuffer) >= self.hyper['trainAfter']:
                    # Updates occur every so often
                    if updateCounter == self.hyper['updateEvery']:

                        # Perform multiple updates together
                        for _ in range(self.hyper['updateEvery']):

                            # Get sample replays
                            reState, reAction, reReward, reNextState, reDone = self.sampleReplay(
                                self.hyper['batchSize'])

                            self.criticOpt.zero_grad()

                            # Q-value of sampled state-action pair
                            value = self.criticModel(torch.cat((reState, reAction), dim=1))

                            with torch.no_grad():  # Don't keep gradients for target networks
                                # Get discounted target Q-value
                                reNextAction = self.actorTarget(reNextState)
                                nextValue = self.criticTarget(
                                    torch.cat((reNextState, reNextAction), dim=1))
                                target = reReward + self.hyper['gamma'] * (1 - reDone) * nextValue

                            # Calculate MSE loss
                            criticLoss = torch.mean((target - value)**2)

                            # Gradient descent to update critic model
                            criticLoss.backward()
                            self.criticOpt.step()

                            # Don't keep track of gradients while updating actor model
                            for p in self.criticModel.parameters():
                                p.requires_grad = False

                            self.actorOpt.zero_grad()

                            # Actor performance measure is Q-value of action from actor network
                            newAction = self.actorModel(reState)
                            # negative loss to perform gradient ascent instead of descent
                            actorLoss = -torch.mean(
                                self.criticModel(torch.cat((reState, newAction), dim=1)))
                            # Gradient ascent to update actor model
                            actorLoss.backward()
                            self.actorOpt.step()

                            # Continue tracking critic model gradients
                            for p in self.criticModel.parameters():
                                p.requires_grad = True

                            # Update target networks weights by tracking the learned network weights
                            with torch.no_grad():  # Don't keep gradients for target networks
                                # Actor
                                for targetP, learnedP in zip(self.actorTarget.parameters(),
                                                             self.actorModel.parameters()):
                                    targetP.data.copy_(self.hyper['trActor'] * learnedP.data +
                                                       (1.0 - self.hyper['trActor']) * targetP.data)
                                # Critic
                                for targetP, learnedP in zip(self.criticTarget.parameters(),
                                                             self.criticModel.parameters()):
                                    targetP.data.copy_(self.hyper['trCritic'] * learnedP.data +
                                                       (1.0 - self.hyper['trCritic']) *
                                                       targetP.data)

                        updateCounter = 0  # Reset counter if update occured

                    updateCounter += 1

                state = nextState
                totalRewards += reward

            # Decay noise term every episode
            self.noiseSigma *= self.hyper['noiseDecay']
            # Decay learning rate every episode
            for group in self.actorOpt.param_groups:
                group['lr'] *= self.hyper['lrDecay']
            for group in self.criticOpt.param_groups:
                group['lr'] *= self.hyper['lrDecay']

            self.trainRewards.append(totalRewards)
            print(f"Episode {self.episode} reward: {totalRewards}")

    def test(self, numEpisodes):
        """
        Test the performance of the model for numEpisodes episodes.
        """

        totalRewards = []  # Store returns from each episode of testing

        for _ in range(numEpisodes):
            state = torch.tensor(self.reset(), device=self.device).float()

            done = False
            totalReward = 0  # return over a single episode
            numSteps = 0  # current step in episode

            while not done:
                numSteps += 1
                # No exploration during testing (noise sigma is 0)
                action = self.chooseAction(state, sigma=0)
                observation, reward, done, info = self.step(action.cpu().numpy())
                totalReward += reward
                state = torch.tensor(observation, device=self.device).float()

            totalRewards.append(totalReward)

        # Store and return mean and standard deviation of test returns
        mean = np.mean(totalRewards)
        std = np.std(totalRewards)

        self.testRewards.append((mean, std))
        print(f"mean: {mean}, std: {std}")
        return (mean, std)
