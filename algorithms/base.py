import matplotlib.pyplot as plt
import numpy as np
import torch
from gym.spaces.dict import Dict


class Base(object):
    """
    This is the base class for deep learning algorithms.
    Algorithms should inherit from this class and implement these Base methods.
    """
    def __init__(self, env, render):
        """Subclasses must call super() to run this constructor"""
        self.env = env  # must be an OpenAI Gym environment
        self.render = render  # Set True to display rendering of environment
        self.hyper = {}  # Hyperparameters listed in a dict
        # Number of state dimensions
        if type(self.env.observation_space) is dict or type(self.env.observation_space) is Dict:
            self.dimState = len(self.env.observation_space['observation'].low)
        else:
            self.dimState = len(self.env.observation_space.low)
        self.dimAction = len(self.env.action_space.low)  # Number of action dimensions
        self.trainRewards = []  # Store rewards from training episodes
        self.testRewards = []  # store rewards from test episodes
        # torch device setting
        # self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.device = torch.device("cpu")

    def reset(self):
        """Override this function to perform a customize the environment reset"""
        return self.env.reset()

    def step(self, action):
        """Override this function to perform a custom environment step"""
        if self.render:
            self.env.render()
        return self.env.step(action)

    def resetModels(self):
        """
        Implement this method to build the neural network models for the particular algorithm.
        This method should also create optimizers and replay buffers if needed.
        """
        raise NotImplementedError

    def train(self, numEpisodes):
        """Implement this method to run numEpisodes episodes of training"""
        raise NotImplementedError

    def test(self, numEpisodes):
        """
        Implement this method to run numEpisodes episodes of testing.
        Typically, no exploration is done in testing
        """
        raise NotImplementedError

    def printRewards(self):
        """Print hyperparameters and rewards to stdout"""
        print(f"----------")
        print(f"Hyperparameters: {self.hyper}")
        print(f"Reward mean, std: {self.testRewards}")
        print(f"----------")

    def saveRewards(self, filename):
        """Save the hyperparameter settings and test reward means and std to a file."""
        f = open('results/' + filename, 'a')
        f.write(f"----------\n")
        f.write(f"Hyperparameters: {self.hyper}\n")
        f.write(f"Reward mean, std: {self.testRewards}\n")
        f.write(f"----------\n")

    def evaluate(self):
        """Alternate between training and testing episodes"""
        trainChunk = int(self.hyper['trainEpisodes'] / 20)
        for _ in range(20):
            self.train(trainChunk)
            self.test(self.hyper['testEpisodes'])

    def plotTestRewards(self, label):
        """Plot means and std devs from test episodes"""
        x = np.arange(len(self.testRewards))
        means = np.array([self.testRewards[i][0] for i in x])
        stds = np.array([self.testRewards[i][1] for i in x])
        x = x * (self.episode / 20)
        # Construct plot
        plt.plot(x, means, label=label)
        plt.fill_between(x, means + stds, means - stds, alpha=0.2)
        plt.xlabel("Result after x episodes")
        plt.ylabel(f"Return per episode")
        plt.title(f"DDPG on LunarLanderContinuous")
        plt.legend()

    def showPlot(self):
        """Call this at the end of the script to display plot"""
        plt.show()