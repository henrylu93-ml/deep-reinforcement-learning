from algorithms.base import Base
import numpy as np

hyper = {}


class RandomAgent(Base):
    def __init__(self, env, render, hyper=hyper):
        super().__init__(env, render)
        self.hyper = hyper

    def resetModels(self):
        self.episode = 0

    def train(self, numEpisodes):

        for _ in range(numEpisodes):
            self.episode += 1

            self.reset()
            done = False
            totalRewards = 0
            numSteps = 0

            while not done:
                numSteps += 1
                observation, reward, done, info = self.step(self.env.action_space.sample())

                totalRewards += reward
                if numSteps >= self.hyper['maxSteps']:
                    done = True

            print(f"Episode {self.episode} total reward: {totalRewards}, steps: {numSteps}")

    def test(self, numEpisodes):
        totalRewards = []

        for _ in range(numEpisodes):
            self.reset()
            done = False
            totalReward = 0  # total reward over a single episode
            numSteps = 0

            while not done:
                numSteps += 1
                observation, reward, done, info = self.step(self.env.action_space.sample())
                totalReward += reward
                if numSteps >= self.hyper['maxSteps']:
                    done = True

            totalRewards.append(totalReward)

        mean = np.mean(totalRewards)
        std = np.std(totalRewards)

        self.testRewards.append((mean, std))
        print(f"mean: {mean}, std: {std}")
        return (mean, std)
